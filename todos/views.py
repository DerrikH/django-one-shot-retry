from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm

# Create your views here.
def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos
    }
    return render(request, "todos/list.html", context)


def show_todo(request, id):
    todos = get_object_or_404(TodoList, id=id)
    # todo_list_list = todos.items.all()
    context = {
        # "todo_list_1": todos,
        "show_todo": todos,
    }
    return render(request, "todos/detail.html", context)

def create_todo(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo= form.save()
        return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def update_todo(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todos)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todos.id)
    else:
        form = TodoForm(instance=todos)
    context = {
        "todos": todos,
        "form": form,
    }
    return render(request, "todos/edit.html", context)

def delete_todo(request, id):
    todos = TodoList.objects.get(id=id)
    if request.method == "POST":
        todos.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo = form.save()
        return redirect("todo_list_detail", id=todo.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/items/create.html", context)

def update_todo_item(request, id):
    todos = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todos)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todos.list.id)
    else:
        form = TodoItemForm(instance=todos)
    context = {
        "todos": todos,
        "form": form,
    }
    return render(request, "todos/items/update.html", context )
